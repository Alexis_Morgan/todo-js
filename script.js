$(() => {

    let todoList = [];

    let selectedColor = null;

    const $todoInput = $('#todo-input');
    const $todoList = $('#todo-list');
    const $addButton = $('#addBtn');
    const $menu = $('#menu');
    const $body = $('body');
    const $ul = $('ul');

    // Random Color

    const generateColor = function () {
        return '#' + Math.floor(Math.random() * 16777215).toString(16)
    }

    // Render

    const display = function () {
        $ul.empty();
        todoList.forEach(todo => {
            $todoList.append(`<li class="todos"  id="${todo.id}" style="background-color:${todo.color}" >
          <input class="checkbox" type="checkbox" value="array" 
            ${todo.status ? 'checked="checked"' : ''} >
            <span class="value" >${todo.value}</span>                
            </li>`);
        });
    }

    // Check

    const checkTodo = function () {

        const thisID = Number($(this)
            .parent()
            .prop('id'));
        todoList.forEach(todo => {
            if (todo.id === thisID) todo.status = this.checked;
        });
        display();
    };

    // Create todo and Add in Array

    const addValue = () => {
        if (selectedColor === null) { selectedColor = generateColor() }
        const todo = {
            id: Math.random(),
            status: false,
            value: $todoInput.val(),
            color: selectedColor
        };


        if (todo.value.trim() !== '' && todo.value.length < 25) {
            todoList.push(todo);

            $todoInput.val('');
            $todoInput.focus();
            $menu
                .find('input[type="checkbox"]')
                .each((index, el) => $(el).removeAttr('checked'));
            selectedColor = null;
            display();
        }
    };

    const enterPush = event => {
        if (event.which === 13) {
            addValue();
        }
    };

    function onColorSelect(event) {
        event.preventDefault();

        if (event.target && event.target.dataset && event.target.dataset.color) {
            $menu
                .find('input[type="checkbox"]')
                .each((index, el) => $(el).removeAttr('checked'));
            const color = event.target.dataset.color;
            const $color = $menu
                .find('span[data-color="' + color + '"]')
                .parent()
                .find('input[type="checkbox"]');
            if (selectedColor !== color) {
                selectedColor = color;
                $color.attr('checked', 'checked');
            } else {
                selectedColor = null;
            }
        }
    }


    $body.on('change', '.checkbox', 'todo.status', checkTodo);
    $addButton.on('click', addValue);
    $todoInput.on('keyup', enterPush);
    $menu.on('click', onColorSelect);
})